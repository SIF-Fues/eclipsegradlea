package com.example.gitgradle.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.gitgradle.domain.EnglishMessageHolder;
import com.example.gitgradle.domain.FrenchMessageHolder;
import com.example.gitgradle.domain.MessageHolder;
import com.example.gitgradle.domain.NotSupportedMessageHolder;

public class GreetingServiceTest {
	
	private GreetingService svc;
	
	@BeforeEach
	public void setUp() {
		svc = new GreetingService();		
	}
	
	@Test
	public void canGreetInEnglish() {
		MessageHolder expected = new EnglishMessageHolder();
		MessageHolder actual = svc.greet("EN");
		assertEquals(expected.getMessage(), actual.getMessage());
	}

	@Test
	public void canGreetInFrench() {
		MessageHolder expected = new FrenchMessageHolder();
		MessageHolder actual = svc.greet("FR");
		assertEquals(expected.getMessage(), actual.getMessage());
	}
	
	@Test
	public void canGreetInNotSupportedLanguage() {
		MessageHolder expected = new NotSupportedMessageHolder();
		MessageHolder actual = svc.greet("FOO");
		assertEquals(expected.getMessage(), actual.getMessage());
	}
}
