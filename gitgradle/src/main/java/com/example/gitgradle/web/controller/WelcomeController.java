package com.example.gitgradle.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.gitgradle.domain.MessageHolder;
import com.example.gitgradle.service.GreetingService;

@RestController
public class WelcomeController {
	
	private GreetingService svc = new GreetingService();
	
	@RequestMapping(path = "/hello")
	public MessageHolder welcome() {
		return svc.greet("FR");
	}	
	
}
