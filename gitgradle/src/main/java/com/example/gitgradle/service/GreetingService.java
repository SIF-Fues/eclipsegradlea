package com.example.gitgradle.service;

import com.example.gitgradle.domain.EnglishMessageHolder;
import com.example.gitgradle.domain.FrenchMessageHolder;
import com.example.gitgradle.domain.MessageHolder;
import com.example.gitgradle.domain.NotSupportedMessageHolder;

public class GreetingService {
	
	public MessageHolder greet(String languageIn) {
		MessageHolder message = new NotSupportedMessageHolder();		
		if("EN".equals(languageIn)) {
			message = new EnglishMessageHolder();
		}

		if("FR".equals(languageIn)) {
			message = new FrenchMessageHolder();
		}
		
		
		return message;		
	}
}
