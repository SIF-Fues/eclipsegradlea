package com.example.gitgradle.domain;

public class FrenchMessageHolder implements MessageHolder {

	@Override
	public String getMessage() {
		return "Bonjour, monsieur et madame!";
	}
}
