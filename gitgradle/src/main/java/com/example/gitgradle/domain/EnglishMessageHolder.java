package com.example.gitgradle.domain;

public class EnglishMessageHolder implements MessageHolder {

	@Override
	public String getMessage() {
		return "Do or DO NOT, There is no try!";
	}
}
